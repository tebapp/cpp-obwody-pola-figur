#include <iostream>
#include <corecrt_math_defines.h>
#include <cmath>
#include <sstream>

using namespace std;
typedef unsigned char byte;

byte menu();
void obwod(byte index);
void pole(byte index);

int main()
{
    while(1) {
        if(menu()==255)
            break;
    }
    return 0;
}

byte menu() {
    byte wybor = 0;
    cout << "Program do obliczen obwodow i pol\n"
            "Obwody:\n1 - kwadrat\n2 - prostokat\n"
            "3 - kolo\n4 - trojkat\nPola:\n"
            "5 - kwadrat\n6 - prostokat\n"
            "7 - kolo\n8 - trojkat\nWartosc x konczy prace.\n"
            "Podaj swoj wybor:";
    cin >> wybor;
    if (wybor == 'x')
        return 255;
    unsigned int a;
    stringstream ss;
    ss << wybor;
    ss >> a;
    if (a < 5)
        obwod(a);
    else {
        pole(a);
    }
    return 0;
}

void obwod(byte wybor) {
    float a,b,c,r,h;

    switch(wybor) {        
        case 1: cout << "Podaj wartosc boku a: ";
                cin >> a;
                cout << "Obwod wynosi: " << a*4 << '\n';
                break;
        case 2: cout << "Podaj wartosc boku a: ";
                cin >> a;
                cout << "Podaj wartosc boku b: ";
                cin >> b;
                cout << "Obwod wynosi: " << a*2+b*2 << '\n';
                break;
        case 3: cout << "Podaj promien: ";
                cin >> r;
                cout << "Obwod wynosi: " << 2*M_PI*r << '\n';
                break;
        case 4: cout<< "Podaj trzy wartosci: ";
                cin >> a >>b >> c;
                cout << "Obwod wynosi: " << a+b+c << "\n";
                break;
        default: cout << "Nie rozpoznano figury\n";
    }
}

void pole(byte wybor) {

}
